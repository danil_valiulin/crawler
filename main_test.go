package main

import (
	"bytes"
	"errors"
	"github.com/stretchr/testify/mock"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestVisitor(t *testing.T) {
	v := &visitor{
		links: make(map[string]struct{}),
	}

	res := v.exists("test")
	require.False(t, res)

	res = v.exists("test")
	require.True(t, res)

	expected := "RESULST: get 1 links"
	require.Equal(t, expected, v.String())
}

func TestParser(t *testing.T) {
	body := ioutil.NopCloser(bytes.NewReader(
		[]byte(`<html><body><a href="http://link.test"></a><a href="/link2"></a></body></html>`)))

	t.Run("success", func(t *testing.T) {
		c := &mockHttpClienter{}
		c.On("Get", mock.AnythingOfType("string")).
			Return(&http.Response{StatusCode: http.StatusOK, Body: body}, nil)

		p := parser{client: c}
		res, err := p.getLinks("http://test.com")
		require.NoError(t, err)
		require.Equal(t, []string{"http://link.test", "http://test.com/link2"}, res)

		c.AssertExpectations(t)
	})

	t.Run("status is not 200", func(t *testing.T) {
		c := &mockHttpClienter{}
		c.On("Get", mock.AnythingOfType("string")).
			Return(&http.Response{StatusCode: http.StatusNotFound, Body: body}, nil)

		p := parser{client: c}
		res, err := p.getLinks("http://test.com")
		require.Error(t, err)
		require.Empty(t, res)

		c.AssertExpectations(t)
	})

	t.Run("client errro", func(t *testing.T) {
		c := &mockHttpClienter{}
		c.On("Get", mock.AnythingOfType("string")).
			Return(&http.Response{StatusCode: http.StatusNotFound, Body: body}, errors.New("some error"))

		p := parser{client: c}
		_, err := p.getLinks("http://test.com")
		require.Error(t, err)

		c.AssertExpectations(t)
	})

	t.Run("not valid link", func(t *testing.T) {
		p := parser{}
		_, err := p.getLinks("%%%%")
		require.Error(t, err)
	})
}

func TestCrawl(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		p := &mockParserer{}
		p.On("getLinks", mock.AnythingOfType("string")).
			Return([]string{"http://1.com", "http:2.com"}, nil)

		v := &mockVisiter{}
		v.On("exists", mock.AnythingOfType("string")).
			Return(false).
			Times(6)

		crawl("http://example.com", p, v, 2)

		p.AssertExpectations(t)
		v.AssertExpectations(t)
	})

	t.Run("parser error", func(t *testing.T) {
		p := &mockParserer{}
		p.On("getLinks", mock.AnythingOfType("string")).
			Return([]string{""}, errors.New("some error")).Once()


		crawl("http://example.com", p, nil, 2)

		p.AssertExpectations(t)
	})
}
