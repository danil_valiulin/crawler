package main

import (
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type parserer interface {
	getLinks(string) ([]string, error)
}

type visiter interface {
	fmt.Stringer
	exists(string) bool
}

type httpClienter interface {
	Get(string) (*http.Response, error)
}

func main() {
	targetFl := flag.String("target", "", "начальная страница для парсинга")
	deptFl := flag.Int("depth", 3, "глубина парсинга")
	flag.Parse()

	target := *targetFl
	depth := *deptFl

	if target == "" {
		flag.Usage()

		return
	}

	_, err := url.Parse(target)
	if err != nil {
		fmt.Println("target url not valid")
		fmt.Printf("reason: %s", err.Error())

		return
	}

	visitor := &visitor{
		links: make(map[string]struct{}),
	}

	client := &http.Client{
		Timeout: 2 * time.Second,
	}
	p := &parser{client: client}
	pool := make(chan struct{}, 10)

	startLinks, err := p.getLinks(target)
	if err != nil {
		panic(err)
	}

	wg := &sync.WaitGroup{}
	for _, l := range startLinks {
		wg.Add(1)
		go func(link string, wg *sync.WaitGroup) {
			defer wg.Done()

			pool <- struct{}{}
			crawl(link, p, visitor, depth)
			<-pool
		}(l, wg)
	}
	wg.Wait()

	fmt.Println("DONE")
	fmt.Println(visitor)
}

func crawl(link string, p parserer, v visiter, deep int) {
	if deep <= 0 {
		return
	}

	links, err := p.getLinks(link)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, l := range links {
		if !v.exists(l) {
			fmt.Println(l)

			crawl(l, p, v, deep-1)
		}
	}
}

type parser struct {
	client httpClienter
}

func (p *parser) getLinks(link string) ([]string, error) {
	urlVal, err := url.Parse(link)
	if err != nil {
		return nil, err
	}

	resp, err := p.client.Get(link)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("response status is no valid: %d", resp.StatusCode)
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, err
	}

	links := make([]string, 0)

	doc.Find("a").Each(func(i int, selection *goquery.Selection) {
		href, ok := selection.Attr("href")
		if !ok {
			return
		}

		if !strings.HasPrefix(href, "http") {
			if strings.HasPrefix(href, "/") {
				href = urlVal.Scheme + "://" + urlVal.Host + href
			} else {
				href = urlVal.Scheme + "://" + urlVal.Host + "/" + href
			}
		}

		links = append(links, href)

	})

	return links, nil
}

type visitor struct {
	links map[string]struct{}
	mux   sync.Mutex
}

func (v *visitor) exists(s string) bool {
	v.mux.Lock()
	defer v.mux.Unlock()

	if _, ok := v.links[s]; !ok {
		v.links[s] = struct{}{}

		return false
	}

	return true
}

func (v *visitor) String() string {
	return fmt.Sprintf("RESULST: get %d links", len(v.links))
}
